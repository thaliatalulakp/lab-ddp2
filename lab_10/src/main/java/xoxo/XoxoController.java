package xoxo;

import xoxo.crypto.XoxoEncryption;
import xoxo.crypto.XoxoDecryption;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 * This class controls all the business
 * process and logic behind the program.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author thalia talula
 */
public class XoxoController {

    /**
     * The GUI object that can be used to get
     * and show the data from and to users.
     */
    private XoxoView gui;

    /**
     * Class constructor given the GUI object.
     */
    public XoxoController(XoxoView gui) {
        this.gui = gui;
    }

    /**
     * Main method that runs all the business process.
     */
    public void run() {
        //TODO: Write your code for logic and everything here
        gui.setEncryptFunction(encryptActLis);
        gui.setDecryptFunction(decryptActLis);
    }

    //TODO: Create any methods that you want
    private ActionListener encryptActLis = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            XoxoEncryption encrypt = new XoxoEncryption(gui.getKeyText());
            String hasil;
            if(gui.getSeedText().equals("DEFAULT_SEED")){
                hasil = encrypt.encrypt(gui.getMessageText(),18).getEncryptedMessage();
            }
            else {
                int intSeed = Integer.parseInt(gui.getSeedText());
                hasil = encrypt.encrypt(gui.getMessageText(), intSeed).getEncryptedMessage();
            }
            gui.appendLog(hasil);
        }
    };

    private ActionListener decryptActLis = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            XoxoDecryption decrypt = new XoxoDecryption(gui.getKeyText());
            String hasil;
            if(gui.getSeedText().equals("DEFAULT_SEED")){
                hasil = decrypt.decrypt(gui.getMessageText(),18);
            }
            else {
                int intSeed = Integer.parseInt(gui.getSeedText());
                hasil = decrypt.decrypt(gui.getMessageText(), intSeed);
            }
            gui.appendLog(hasil);
        }
    };

}