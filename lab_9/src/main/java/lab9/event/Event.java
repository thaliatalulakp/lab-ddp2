package lab9.event;

import java.math.BigInteger;
import java.text.*;
import java.util.Date;
/**
* A class representing an event and its properties
*/
public class Event implements Comparable<Event>{
    /** Name of event */
    // TODO: Make instance variables for representing beginning and end time of event
    // TODO: Make instance variable for cost per hour
    private String name;
    private Date startTime;
    private Date endTime;
    private BigInteger costPerHour;
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");

    // TODO: Create constructor for Event class
    public Event (String name, String startTime, String endTime, BigInteger costPerHour){
        this.name = name;
        try {
            this.startTime = format.parse(startTime);
            this.endTime = format.parse(endTime);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.costPerHour = costPerHour;
    }
    
    /**
    * Accessor for name field. 
    * @return name of this event instance
    */
    public String getName() {return this.name;}

    /**
     * Accessor for name field.
     * @return costPerHour
     */
    public BigInteger getCost() {return this.costPerHour;}

    /**
     * overlapsWith method, untuk mengecek apakah event berlangsung bersamaan dengan event lain atau tidak
     * @param other, other event
     * @return true jika event overlaps
     * @return false jika event tidak overlaps
     */
    public boolean overlapsWith(Event other){
        if (other.startTime.after(this.startTime) && other.startTime.before(this.endTime)) {
            return true;
        }
        else if (other.endTime.after(this.startTime) && other.endTime.before(this.endTime)) {
            return true;
        }
        return false;
    }

    // TODO: Implement toString()
    // HINT: Implement a method to test if this event overlaps with another event
    //       (e.g. boolean overlapsWith(Event other)). This may (or may not) help
    //       with other parts of the implementation.

    /**
     * toString method, untuk mencetak deskripsi event
     * @return name, start & end time, cost
     */
    public String toString(){
        SimpleDateFormat formatOut = new SimpleDateFormat("dd-MM-yyyy, HH:mm:ss");
        return this.getName() +
                "\nWaktu mulai: " + formatOut.format(this.startTime) +
                "\nWaktu selesai: " + formatOut.format(this.endTime) +
                "\nBiaya kehadiran: " + this.getCost();
    }

    /**
     * compareTo method, untuk compare starttime event dengan starttime event lainnya
     * @param other
     * @return
     */
    @Override
    public int compareTo(Event other) {
        return this.startTime.compareTo(other.startTime);
    }
}
