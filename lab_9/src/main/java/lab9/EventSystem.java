package lab9;

import lab9.user.User;
import lab9.event.Event;
import java.math.BigInteger;
import java.util.ArrayList;
import java.text.*;
import java.util.Date;

/**
* Class representing event managing system
*/
public class EventSystem {
    /**
    * List of events
    */
    private ArrayList<Event> events;
    
    /**
    * List of users
    */
    private ArrayList<User> users;

    /**
    * Constructor. Initializes events and users with empty lists.
    */
    public EventSystem(){
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }

    /**
     * getEvent method, untuk mengecek apakah name ada dilist events atau tidak
     * @param name, nama event yang akan dicek
     * @return event jika ada dilist, null jika tidak ada dilist
     */
    public Event getEvent(String name){
        for (Event elemen : events){
            if (elemen.getName().equals(name)){
                return elemen;
            }
        }
        return null;
    }

    /**
     * getUser method, untuk mengecek apakah name ada dilist users atau tidak
     * @param name, nama user yang akan dicek
     * @return user jika ada dilist, null jika tidak ada dilist
     */
    public User getUser(String name){
        for (User elemen : users){
            if (elemen.getName().equals(name)){
                return elemen;
            }
        }
        return null;
    }

    /**
     * addEvent method, untuk add event ke list events
     *
     * @param name
     * @param startTimeStr
     * @param endTimeStr
     * @param costPerHourStr
     * @return add event ke list events jika waktu valid
     * @return mencetak string yang menyatakan waktu tidak valid, jika waktu tidak valid
     */
    public String addEvent(String name, String startTimeStr, String endTimeStr,  String costPerHourStr) {
        // TODO: Implement!
        if (getEvent(name) == null){ //kalau belum ada name dilist events
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
            BigInteger bigCost= new BigInteger(costPerHourStr);
            try {
                Date dateStart = format.parse(startTimeStr);
                Date dateEnd = format.parse(endTimeStr);

                if (dateStart.before(dateEnd)){
                    Event event = new Event(name, startTimeStr, endTimeStr, bigCost);
                    events.add(event);
                    return "Event " + name + " berhasil ditambahkan!";
                }
                else{
                    return "Waktu yang diinputkan tidak valid!";
                }
            } catch (ParseException e){
                e.printStackTrace();
                return "";
            }
        }
        else {
            return "Event " + name + " sudah ada!";
        }
    }

    /**
     * addUser method, untuk add user ke list users
     * @param name, nama user
     * @return add user ke list users jika belum ada name dilist users
     * @return mencetak string yang menyatakan name sudah ada jika sudah ada name di list users
     */
    public String addUser(String name){
        // TODO: Implement:
        if (getUser(name) == null){
            User user = new User(name);
            users.add(user);
            return "User " + name + " berhasil ditambahkan!";
        }
        else {
            return "User " + name + " sudah ada!";
        }
    }

    /**
     * registerToEvent method, untuk user add event ke list events
     * @param userName, nama user
     * @param eventName, nama event
     * @return mencetak statement
     */
    public String registerToEvent(String userName, String eventName){
        // TODO: Implement
        User user = getUser(userName);
        Event event = getEvent(eventName);
        if (user == null && event == null) { //kalau tidak ada user dan event
            return "Tidak ada pengguna dengan nama " + userName + " dan acara dengan nama " + eventName + "!";
        }
        else {
            if (user != null) {
                if (event != null) {
                    if (user.addEvent(event)){
                        user.getEvents().add(event);
                        return userName + " berencana menghadiri " + eventName + "!";
                    }
                    else{
                        return userName + " sibuk sehingga tidak dapat menghadiri " + eventName + "!";
                    }
                }
                else {
                    return "Tidak ada acara dengan nama " + eventName + "!";
                }
            }
            else {
                return "Tidak ada pengguna dengan nama " + userName + "!";
            }
        }
    }
}