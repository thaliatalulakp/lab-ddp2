import java.util.Scanner;

class Lab8 {
    //main program
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int maxGaji = Integer.parseInt((scanner.nextLine()));
        Staff.setGajiPromosi(maxGaji);

        while (true) {
            String[] input = scanner.nextLine().split(" ");
            if (input[0].equals("TAMBAH_KARYAWAN")) {
                String nama = input[2];
                String tipe = input[1];
                int gaji = Integer.parseInt(input[3]);
                tambahKaryawan(nama, tipe, gaji);
            } else if (input[0].equals("STATUS")) {
                String nama = input[1];
                statusKaryawan(nama);
            } else if (input[0].equals("TAMBAH_BAWAHAN")) {
                String namaMenambah = input[1];
                String namaDitambah = input[2];
                tambahBawahan(namaMenambah, namaDitambah);
            } else if (input[0].equals("GAJIAN")) {
                gajian();
            }
            else {
                break;
            }
        }
    }

    //method untuk mencari nama karyawan di list yang berisi nama karyawan
    public static Karyawan find(String nama){
        for (Karyawan elemen : Karyawan.getKorporasi()){
            if (elemen.getNama().equals(nama)){
                return elemen;
            }
        }
        return null;
    }

    //method untuk menambah karyawan
    public static void tambahKaryawan(String nama, String tipe, int gaji){
        if (find(nama) == null) { //kalau tidak ada nama yang dicari
            if (tipe.equals("MANAGER")) {
                Karyawan karyawanManager = (Karyawan) new Manager(nama, gaji);
                Karyawan.tambahKeKorporasi(karyawanManager);
                System.out.println(nama + " mulai bekerja sebagai " + tipe + " di PT. TAMPAN");
            }
            else if (tipe.equals("STAFF")) {
                Karyawan karyawanStaff = (Karyawan) new Staff(nama, gaji);
                Karyawan.tambahKeKorporasi(karyawanStaff);
                System.out.println(nama + " mulai bekerja sebagai " + tipe + " di PT. TAMPAN");
            }
            else if (tipe.equals("INTERN")) {
                Karyawan karyawanIntern = (Karyawan) new Intern(nama, gaji);
                Karyawan.tambahKeKorporasi(karyawanIntern);
                System.out.println(nama + " mulai bekerja sebagai " + tipe + " di PT. TAMPAN");
            }
        }
        else { //kalau nama sudah ada di list karyawan
            System.out.println("Karyawan dengan nama " + nama + " telah terdaftar");
        }
    }

    //method untuk mencetak status karyawan
    public static void statusKaryawan(String nama) {
        if (find(nama) != null) { //ada karyawan
            nama = nama.toLowerCase();
            System.out.println(nama + " " + Karyawan.getGaji());
        }
        else {
            System.out.println("Karyawan tidak ditemukan");
        }
    }

    //method untuk menambah bawahan karyawan
    public static void tambahBawahan(String namaMenambah, String namaDitambah){
        Karyawan karyawanMenambah = find(namaMenambah);
        Karyawan karyawanDitambah = find(namaDitambah);
        if (karyawanMenambah != null && karyawanDitambah != null){ //kalau ada nama karyawan yang dicari
            if (karyawanMenambah instanceof Manager){
                if (karyawanDitambah instanceof Staff || karyawanDitambah instanceof Intern) {
                    ((Manager) karyawanMenambah).bawahanKaryawan(karyawanDitambah);
                    System.out.println("Karyawan " + namaDitambah + " berhasil ditambahkan menjadi bawahan " + namaMenambah);
                }
            }
            else if (karyawanMenambah instanceof Staff){
                if (karyawanDitambah instanceof Intern){
                    ((Staff) karyawanMenambah).bawahanKaryawan(karyawanDitambah);
                    System.out.println("Karyawan " + namaDitambah + " berhasil ditambahkan menjadi bawahan " + namaMenambah);
                }
            }
            else { //kalau karyawan tersebu tidak memiliki hak untuk menambah karyawan
                System.out.println("Anda tidak layak memiliki bawahan");
            }
        }
        else { //kalau tidak ada nama karyawan yang dicari
            System.out.println("Nama tidak berhasil ditemukan");
        }
    }

    //method untuk menghitung dan mencetak gaji karyawan
    public static void gajian(){
        System.out.println("Semua karyawan telah diberikan gaji");
        for (Karyawan elemen : Karyawan.getKorporasi()){
            elemen.setCount(elemen.getCount() + 1);
            if (elemen.getCount() == 6){ //untuk mendapatkan kenaikan gaji
                int gajiUpdate = (elemen.getGaji()*110)/100;
                int gajiSblmUpdate = (elemen.getGaji()*100)/110;
                elemen.setGaji(gajiUpdate);
                System.out.println(elemen.getNama() + " mengalami kenaikan gaji sebesar 10% dari " + gajiSblmUpdate + " menjadi " + gajiUpdate);
            }
        }
    }
}