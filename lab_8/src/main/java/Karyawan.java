import java.util.ArrayList;

public class Karyawan extends Korporasi{
    private static ArrayList<Karyawan> korporasi = new ArrayList<Karyawan>();

    public Karyawan(String nama, int gaji){
        this.nama = nama;
        this.gaji = gaji;
    }

    public static ArrayList<Karyawan> getKorporasi() {return korporasi;}

    public void setKorporasi(ArrayList<Karyawan> korporasi) {this.korporasi = korporasi;}

    //method untuk menambah karyawan ke korporasi
    public static void tambahKeKorporasi(Karyawan nama){
        if (korporasi.size() <= 10000){
            korporasi.add(nama);
        }
        else {
            System.out.println("Tidak dapat menambah karyawan lagi");
        }
    }

    //method untuk mengubah gaji
    public void setGaji(int gaji){
        if (this instanceof Staff){
            if (this.gaji > Staff.getGaji()){ //kalau gaji karyawan melebihi batas gaji yang diinput
                Karyawan jabatanBaru = new Manager(nama , gaji);
                korporasi.set(korporasi.indexOf(this), jabatanBaru); //mengubah jabatan menjadi manager
                System.out.println("Selamat, " + jabatanBaru.getNama() + " telah dipromosikan menjadi MANAGER");
            }
        }
    }
}
