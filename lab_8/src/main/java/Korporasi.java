public abstract class Korporasi {
    protected static String nama;
    protected static int gaji;
    protected int count;

    public static String getNama() {return nama;}

    public static void setNama(String nama) {Korporasi.nama = nama;}

    public static int getGaji() {return gaji;}

    public void setGaji(int gaji) {Korporasi.gaji = gaji;}

    public int getCount() {return count;}

    public void setCount(int count) {this.count = count;}
}
