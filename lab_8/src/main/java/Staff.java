import java.util.ArrayList;

public class Staff extends Karyawan{
    private ArrayList<Karyawan> listStaff = new ArrayList<>();
    private static int gajiPromosi;

    public Staff(String nama, int gaji){
        super(nama, gaji);
    }

    public ArrayList<Karyawan> getListStaff() {return listStaff;}

    public int getGajiPromosi() {
        return gajiPromosi;
    }

    public static void setGajiPromosi(int gajiPromosi) {
        Staff.gajiPromosi = gajiPromosi;
    }

    //method untuk menambah bawahan staff
    public void bawahanKaryawan(Karyawan staff){
        if (listStaff.size() <= 10){
            if (staff instanceof Intern) {
                listStaff.add(staff);
            }
        }
    }
}
