import java.util.ArrayList;

public class Manager extends Karyawan{
    private ArrayList<Karyawan> listManager = new ArrayList<>();

    public Manager(String nama, int gaji){
        super(nama, gaji);
    }

    public ArrayList<Karyawan> getListManager() {return listManager;}

    //method untuk menambah bawahan manager
    public void bawahanKaryawan(Karyawan manager){
        if (listManager.size() <= 10){
            if (manager instanceof Staff || manager instanceof Intern) {
                listManager.add(manager);
            }
        }
    }
}
