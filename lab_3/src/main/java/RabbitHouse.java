import java.util.Scanner;

public class RabbitHouse{
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("masukkan tipe dan nama kelinci: ");
		String[] kelinci = input.nextLine().split(" ");
		int jmlHuruf = kelinci[1].length();	

		if (kelinci[0].equals("palindrome")){
			String nama = kelinci[1];
			System.out.println(RabbitHouse.ujiPalindrome(nama)); 	
		}
		else{
			System.out.println(RabbitHouse.rabbit(1, jmlHuruf));
		}

		if (jmlHuruf >= 10){
			System.out.println("nama kelinci tidak boleh lebih dari 10 huruf");	
		}
		 
		input.close();
	}

	
	public static int rabbit(int jmlKelinci, int jmlHuruf){
		if (jmlHuruf < 1){
			return 0;
		}
		
		else {
			return jmlKelinci + rabbit(jmlKelinci*jmlHuruf, jmlHuruf-1);
		}

	}

	public static int ujiPalindrome(String nama){
		String reverse = new StringBuffer(nama).reverse().toString();
		int hasil = 0;
		int jmlHuruf = nama.length();
		
		if (jmlHuruf < 1){
			hasil=0;
		}

		else if (nama.equals(reverse) == true ){
			hasil=0;
		}

		else{
			hasil++;
			for(int i = 0; i<nama.length(); i++){
				hasil += ujiPalindrome(nama.substring(0,i) + nama.substring(i+1, nama.length()));
			}
		}

		return hasil;
	}
}
