import java.util.Scanner;

public class MainBingo{
	public static void main(String[] args){
		
		Number[][] numbers = new Number [5][5];
		Number[] numberStates = new Number [100];
		Scanner input = new Scanner(System.in);
		

		for(int row=0; row<5; row++){
			String[] inputAngka = input.nextLine().split(" ");
			for(int col=0; col<5; col++){
				numbers[row][col] = new Number(Integer.parseInt(inputAngka[col]), row, col);
				numberStates[Integer.parseInt(inputAngka[col])] = numbers[row][col];
			}
		}

		BingoCard bingo = new BingoCard(numbers, numberStates);
		while(!bingo.isBingo()){
			String command = input.next();
		if(command.equals("RESTART")){
			bingo.restart();
			continue;
		}
		else if(command.equals("INFO")){
			System.out.println(bingo.info());
			continue;
		}
		else if(command.equals("MARK")){
			int angka = input.nextInt();
			System.out.println(bingo.markNum(angka));
			if(bingo.isBingo()){
				System.out.println("BINGO!");
				System.out.println(bingo.info());
			}
		}
		else{
			System.out.println("Incorrect command");
		}
		}
	}
}