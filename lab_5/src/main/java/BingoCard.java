/**
 * BingoCard Template created by Nathaniel Nicholas
 * Template untuk mengerjakan soal bonus tutorial lab 5
 * Template ini tidak wajib digunakan
 * Side Note : Jangan lupa untuk membuat class baru yang memiliki method main untuk menjalankan program dengan spesifikasi yang diharapkan
 */
//import java.util.Scanner;


public class BingoCard {

	private Number[][] numbers;
	private Number[] numberStates; 
	private boolean isBingo;
	
	//contructor
	public BingoCard(Number[][] numbers, Number[] numberStates) {
		this.numbers = numbers;
		this.numberStates = numberStates;
		this.isBingo = false;
	}
	
	//method getter dan setter
	public Number[][] getNumbers() {
		return numbers;
	}

	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}

	public Number[] getNumberStates() {
		return numberStates;
	}

	public void setNumberStates(Number[] numberStates) {
		this.numberStates = numberStates;
	}	

	public boolean isBingo() {
		return isBingo;
	}

	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}

	//membuat method markNum
	public String markNum(int num){
		String hasil = "";
		if(this.numberStates[num] == null){ //untuk angka yang tidak ada di dalam bingo
			hasil += "Kartu tidak memiliki angka " + num;		
		}
		
		else if(this.numberStates[num].isChecked()){ //untuk angka yang sudah tersilang 
			hasil += num + " sebelumnya sudah tersilang";
		}
		else if(this.numberStates[num].isChecked() == false){ //untuk angka yang belum tersilang
			int x = this.numberStates[num].getX();
			int y = this.numberStates[num].getY();
			this.numberStates[num].setChecked(true);
			this.numbers[x][y].setChecked(true);
			hasil += num + " tersilang";

			for(int hor=0; hor<5; hor++){ //untuk mengecek bingo horizontal
				if (this.numbers[hor][0].isChecked()  &&  this.numbers[hor][1].isChecked()  &&  this.numbers[hor][2].isChecked()  &&  this.numbers[hor][3].isChecked()  &&  this.numbers[hor][4].isChecked()){
					this.setBingo(true);
					break;
				}
			}

			for(int ver=0; ver<5; ver++){ //untuk mengecek bingo vertikal
				if (this.numbers[0][ver].isChecked()  &&  this.numbers[1][ver].isChecked()  &&  this.numbers[2][ver].isChecked()  &&  this.numbers[3][ver].isChecked()  &&  this.numbers[4][ver].isChecked()){
					this.setBingo(true);			
					break;
				}
			}
			
			//untuk mengecek bingo diagonal
			if (this.numbers[0][0].isChecked()  &&  this.numbers[1][1].isChecked()  &&  this.numbers[2][2].isChecked()  &&  this.numbers[3][3].isChecked()  &&  this.numbers[4][4].isChecked()){
				this.setBingo(true);					
			}
			
			//untuk mengecek bingo diagonal	
			else if (this.numbers[0][4].isChecked()  &&  this.numbers[1][3].isChecked()  &&  this.numbers[2][2].isChecked()  &&  this.numbers[3][1].isChecked()  &&  this.numbers[4][0].isChecked()){
				this.setBingo(true);			
			}
		}
		return hasil;
	}	
	
	//membuat method info
	public String info(){
		String hasil = "";
		for(int row=0; row<numbers.length; row++){
			for(int col=0; col<numbers[row].length; col++){
				if (this.numbers[row][col].isChecked()){
					hasil += "| X  ";
				}
				else{
					hasil += "| " + this.numbers[row][col].getValue() + " ";
				}
			}
			if(row!=4){
				hasil += "|\n";	
			}
			else{
				hasil += "|";	
			}
		}
		return hasil;
	}

	//membuat method restart
	public void restart(){
		for(int row = 0; row < numbers.length; row ++){
			for(int col = 0; col < numbers[row].length; col ++){
				this.numbers[row][col].setChecked(false);
			}
		}
		System.out.println("Mulligan!");
	}
}

