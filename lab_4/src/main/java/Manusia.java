public class Manusia{
	private String nama;
	private int umur;
	private int uang;
	private float kebahagiaan;
	private static final float KEBAHAGIAAN_MIN = 0;
	private static final float KEBAHAGIAAN_MAX = 100;
	private static final float KEBAHAGIAAN_DEFAULT = 50;
	private static final int UANG_DEFAULT = 50000;

	public Manusia(String nama, int umur, int uang, float kebahagiaan){
		this.nama = nama;
		this.umur = umur;
		this.uang = uang;
		this.kebahagiaan = kebahagiaan;
	}

	public Manusia(String nama, int umur){
		this.nama = nama;
		this.umur = umur;
		this.uang = UANG_DEFAULT;
		this.kebahagiaan = KEBAHAGIAAN_DEFAULT;	
	}

	public Manusia(String nama, int umur, int uang){
		this.nama = nama;
		this.umur = umur;
		this.uang = uang;
		this.kebahagiaan = KEBAHAGIAAN_DEFAULT;
	}

	public Manusia(String nama, int umur, float kebahagiaan){
		this.nama = nama;
		this.umur = umur;
		this.uang = UANG_DEFAULT;
		this.kebahagiaan = kebahagiaan;
	}

	public String getNama(){
		return nama;
	}

	public void setNama(String nama){
		this.nama = nama;
	}

	public int getUmur(){
		return umur;
	}

	public void setUmur(int umur){
		this.umur = umur;
	}

	public int getUang(){
		return uang;
	}

	public void setUang(int uang){
		this.uang = uang;
	}

	public float getKebahagiaan(){
		return kebahagiaan;
	}

	public void setKebahagiaan(float kebahagiaan){
		if (kebahagiaan < KEBAHAGIAAN_MIN){
			this.kebahagiaan = KEBAHAGIAAN_MIN;
		}
		
		else if (kebahagiaan > KEBAHAGIAAN_MAX){
			this.kebahagiaan = KEBAHAGIAAN_MAX;
		}
		else{
			this.kebahagiaan = kebahagiaan;
		}
	}

	public void beriUang(Manusia penerima){
		int ascii = 0;
		for (int i=0; i<penerima.nama.length(); i++) {
			char character = penerima.nama.charAt(i);
			int characterInt = (int)character;
			ascii += characterInt;
		}	
		int jumlahUang = (ascii*100);

		if(jumlahUang <= this.uang){
			float kebahagiaanBaru = (this.kebahagiaan + ((float)jumlahUang / 6000));
			this.setKebahagiaan(kebahagiaanBaru);
			penerima.setKebahagiaan((float)(penerima.kebahagiaan + ((float)jumlahUang / 6000)));
			System.out.println(this.nama + " memberi uang sebanyak " + jumlahUang + " kepada " + penerima.nama + " mereka berdua senang :D");
			this.uang -= jumlahUang;
			penerima.uang += jumlahUang;
		}

		else{
			System.out.println(this.nama + " ingin memberi uang kepada " + penerima.nama + " namun tidak memiliki cukup uang :'(");
		}
		
	}

	public void beriUang(Manusia penerima, int jumlahUang){
		if(jumlahUang <= this.uang){
			float kebahagiaanBaru = (this.kebahagiaan + ((float)jumlahUang / 6000));
			this.setKebahagiaan(kebahagiaanBaru);
			penerima.setKebahagiaan((float)(penerima.kebahagiaan + ((float)jumlahUang / 6000)));
			System.out.println(this.nama + " memberi uang sebanyak " + jumlahUang + " kepada " + penerima.nama + " mereka berdua senang :D");
			this.uang -= jumlahUang;
			penerima.uang += jumlahUang;
		}

		else{
			System.out.println(this.nama + " ingin memberi uang kepada " + penerima.nama + " namun tidak memiliki cukup uang :'(");
		}
	}

	public void bekerja(int durasi, int bebanKerja){
		if (this.umur < 18){
			System.out.println(this.nama + " belum boleh bekerja karena masih dibawah umur D:");
		}
	
		else{
			int bebanKerjaTotal = durasi * bebanKerja;
			int pendapatan = 0;

			if (bebanKerjaTotal <= this.kebahagiaan){
				float kebahagiaanBaru = this.kebahagiaan - bebanKerjaTotal;
				this.setKebahagiaan(kebahagiaanBaru);
				pendapatan = bebanKerjaTotal * 10000;
				System.out.println(this.nama + " bekerja full time, total pendapatan : " + pendapatan);
			}
			else{
				int durasiBaru = ((int) this.kebahagiaan / bebanKerja);
				bebanKerjaTotal = durasiBaru * bebanKerja;
				pendapatan = bebanKerjaTotal * 10000;
				float kebahagiaanBaru = this.kebahagiaan - bebanKerjaTotal;
				
				if(kebahagiaanBaru < 0){
					this.setKebahagiaan(KEBAHAGIAAN_MIN);
				}
				else{
					this.setKebahagiaan(kebahagiaanBaru);
				}
			
				System.out.println(this.nama + " tidak bekerja full time karena sudah terlalu lelah, total pendapatan : " + pendapatan);
			}
			this.uang += pendapatan;
		}
	}

	public void rekreasi(String namaTempat){
		int biaya = namaTempat.length() * 10000;
		if (this.uang >= biaya){
			float kebahagiaanBaru = this.kebahagiaan + namaTempat.length();
			this.setKebahagiaan(kebahagiaanBaru);
			System.out.println(this.nama + " berekreasi di " + namaTempat + " , " + this.nama + " senang :)");
			this.uang -= biaya;
		}

		else{
			System.out.println(this.nama + " tidak mempunyai cukup uang untuk berekreasi di " + namaTempat + " :(");
		}
	}

	public void sakit(String namaPenyakit){
		float kebahagiaanBaru = this.kebahagiaan - namaPenyakit.length();
		this.setKebahagiaan(kebahagiaanBaru);
		System.out.println(this.nama + " terkena penyakit " + namaPenyakit + " :O ");
	}

	public String toString(){
		return "Nama\t\t: " + this.nama +
				"\nUmur\t\t: " + this.umur +
				"\nUang\t\t: " + this.uang +
				"\nKebahagiaan\t: " + this.kebahagiaan;
	}

}