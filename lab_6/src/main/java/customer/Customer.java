//Put your Customer class here, and don't forget to write "package customer" at the top of your program
package customer;
import movie.Movie;
import theater.Theater;
import ticket.Ticket;
import customer.Customer;

public class Customer{
	private String nama;
	private int umur;
	private String isPerempuan;

	public Customer(String nama, String isPerempuan, int umur){
		this.nama = nama;
		this.umur = umur;
		this.isPerempuan = isPerempuan;
	}

	public void findMovie(Theater namaBioskop, String film) {
        int jumlah = 0;
        for(Movie namaFilm : namaBioskop.getFilm()) {
            if (namaFilm.getJudulFilm().equals(film)) {
                System.out.println("\n------------------------------------------------------------------");
                System.out.println("Judul   : " + namaFilm.getJudulFilm());
                System.out.println("Genre   : " + namaFilm.getGenre());
                System.out.println("Durasi  : " + namaFilm.getDurasi() + " menit");
                System.out.println("Rating  : " + namaFilm.getRating());
                System.out.print("Jenis   : Film " + namaFilm.getJenis());
                System.out.println("\n------------------------------------------------------------------");
                break;
            } 

            else {
                jumlah += 1;
            }
        }
        if (jumlah == namaBioskop.getFilm().length) {
            System.out.print("Film " + film + " yang dicari " + this.nama + " tidak ada di bioskop " + namaBioskop.getNamaTheater());
        }
    }

    public Ticket orderTicket(Theater namaBioskop, String film, String hari, String jenis) {
        int jumlah = 0;
        Ticket beliTiket = null;
        for (Ticket tiket : namaBioskop.getJmlTiket()) {
            String aNama = tiket.getJudul().getJudulFilm();
            String aHari = tiket.getHari();
            String aJenis = tiket.isIs3D();
            if (aNama.equals(film) && aHari.equals(hari) && aJenis.equals(jenis)) {
                if(this.umur >= tiket.getJudul().MinimalUmur()) {
                    System.out.println(this.nama + " telah membeli tiket " + aNama + " jenis " + aJenis + " di " + namaBioskop.getNamaTheater() + " pada hari " + tiket.getHari() + " seharga Rp. " + tiket.hargaTiket());
                    namaBioskop.setSaldo(namaBioskop.getSaldo() + tiket.hargaTiket());
                    beliTiket = tiket;
                    break;
                } 
                else {
                    System.out.println(this.nama + " masih belum cukup umur untuk menonton " + tiket.getJudul().getJudulFilm() + " dengan rating " + tiket.getJudul().getRating());
                    break;
                }
            } 
            else {
                jumlah += 1;
            }
        }
        if (jumlah == namaBioskop.getJmlTiket().size()) {
            System.out.println("Tiket untuk film " + film + " jenis " + jenis + " dengan jadwal " + hari + " tidak tersedia di " + namaBioskop.getNamaTheater());
        }
        return beliTiket;
    }
}