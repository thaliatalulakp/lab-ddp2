//Put your Ticket class here, and don't forget to write "package ticket" at the top of your program
package ticket;
import movie.Movie;
import theater.Theater;
import ticket.Ticket;
import customer.Customer;

public class Ticket{
	private Movie judul;
	private String hari;
	private boolean is3D;
	private final int HARGA = 60000;

	public Ticket(Movie judul, String hari, boolean is3D){
		this.judul = judul;
		this.hari = hari;
		this.is3D = is3D;
	}

	public Movie getJudul(){
		return judul;
	}

	public void setJudul(Movie judul){
		this.judul = judul;
	}

	public String getHari(){
		return hari;
	}

	public void setHari(String hari){
		this.hari = hari;
	}

	public String isIs3D(){
		if (is3D){
			return "3 Dimensi";
		}
		else{
			return "Biasa";
		}
	}

	public int hargaTiket(){
		int harga = HARGA;
		if (hari.equals("Sabtu") || hari.equals("Minggu")){
			harga += 40000;
		}
		if (is3D){
			harga += (int)(harga*0.2);
		}
		return harga;
	}
}