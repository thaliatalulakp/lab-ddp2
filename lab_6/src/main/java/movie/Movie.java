//Put your Movie class here, and don't forget to give the package name "movie"
package movie;
import movie.Movie;
import theater.Theater;
import ticket.Ticket;
import customer.Customer;

public class Movie{
	private String judulFilm;
	private String rating;
	private int durasi;
	private String genre;
	private String jenis;
	private int minUmur;

	public Movie(String judulFilm, String rating, int durasi, String genre, String jenis){
		this.judulFilm = judulFilm;
		this.rating = rating;
		this.durasi = durasi;
		this.genre = genre;
		this.jenis = jenis;
	}

	public String getJudulFilm(){
		return judulFilm;
	}

	public void setJudulFilm(String judulFilm){
		this.judulFilm = judulFilm;
	}

	public String getRating(){
		return rating;
	}

	public void setRating(String rating){
		this.rating = rating;
	}

	public int getDurasi(){
		return durasi;
	}

	public void setDurasi(int durasi){
		this.durasi = durasi;
	}

	public String getGenre(){
		return genre;
	}

	public void setGenre(String genre){
		this.genre = genre;
	}

	public String getJenis(){
		return jenis;
	}

	public void setJenis(String jenis){
		this.jenis = jenis;
	}

	public int getMinUmur(){
		return minUmur;
	}

	public int MinimalUmur(){
		switch (this.rating){
			case "Dewasa":
				return 17;
			case "Remaja":
				return 13;
			default:
				return 0;
		}
	}
}