//Put your Theater class here, and don't forget to write "package theater" at the top of your program
package theater;
import java.util.ArrayList;
import movie.Movie;
import theater.Theater;
import ticket.Ticket;
import customer.Customer;

public class Theater{
	private String namaTheater;
	private Movie[] film;
	private ArrayList jmlTiket;
	private int saldo;

	public Theater(String namaTheater, int saldo, ArrayList jmlTiket, Movie[] film){
		this.namaTheater = namaTheater;
		this.saldo = saldo;
		this.jmlTiket = jmlTiket;
		this.film = film;
	}

	public String getNamaTheater(){
		return namaTheater;
	}

	public void setNamaTheater(String namaTheater){
		this.namaTheater = namaTheater;
	}

	public Movie[] getFilm(){
		return film;
	}

	public void setFilm(Movie[] film){
		this.film = film;
	}

	public ArrayList<Ticket> getJmlTiket(){
		return jmlTiket;
	}

	public void setJmlTiket(ArrayList<Ticket> jmlTiket){
		this.jmlTiket = jmlTiket;
	}

	public int getSaldo(){
		return saldo;
	}

	public void setSaldo(int saldo){
		this.saldo = saldo;
	}

	public void printInfo(){
		System.out.println("------------------------------------------------------------------");
		System.out.println("Bioskop                 : " + namaTheater);
		System.out.println("Saldo Kas               : " + saldo);
		System.out.println("Jumlah tiket tersedia   : " + jmlTiket.size());
		System.out.print("Daftar Film tersedia    : ");

		for(int i = 0 ; i < film.length ; i++){
			if (i == film.length - 1){
				System.out.print(film[i].getJudulFilm());
			}
			else{
				System.out.print(film[i].getJudulFilm() + ", ");
			}
		}
		System.out.println("\n------------------------------------------------------------------");

	}

	public static void printTotalRevenueEarned(Theater[] daftarTheater){
		int revenue = 0;
		for (Theater namaBioskop : daftarTheater){
			revenue += namaBioskop.getSaldo();
		}

		System.out.println("Total uang yang dimiliki Koh Mas : Rp." + revenue);
		System.out.println("------------------------------------------------------------------");

		for (Theater namaBioskop : daftarTheater){
			System.out.println("Bioskop         : " + namaBioskop.getNamaTheater());
			System.out.println("Saldo Kas       : " + namaBioskop.getSaldo() + "\n");
		}
		System.out.println("------------------------------------------------------------------");

	}
}