import java.util.Scanner;

/**
 * @author Template Author: Ichlasul Affan dan Arga Ghulam Ahmad
 * Template ini digunakan untuk Tutorial 02 DDP2 Semester Genap 2017/2018.
 * Anda sangat disarankan untuk menggunakan template ini.
 * Namun Anda diperbolehkan untuk menambahkan hal lain berdasarkan kreativitas Anda
 * selama tidak bertentangan dengan ketentuan soal.
 *
 * Cara penggunaan template ini adalah:
 * 1. Isi bagian kosong yang ditandai dengan komentar dengan kata TODO
 * 2. Ganti titik-titik yang ada pada template agar program dapat berjalan dengan baik.
 *
 * Code Author (Mahasiswa):
 * @author ....., NPM ....., Kelas ....., GitLab Account: .....
 */

public class SistemSensus {
	public static void main(String[] args) {
		// Buat input scanner baru
		Scanner input = new Scanner(System.in);


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// User Interface untuk meminta masukan
		System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
				"--------------------\n" +
				"Nama Kepala Keluarga   : ");
		String nama = input.nextLine();
		System.out.print("Alamat Rumah           : ");
		String alamat = input.nextLine();
		System.out.print("Panjang Tubuh (cm)     : ");
		int panjang = Integer.parseInt(input.nextLine());
		if (0<panjang && panjang<=250) {
			panjang = panjang;
		}
		else {
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		System.out.print("Lebar Tubuh (cm)       : ");
		int lebar = Integer.parseInt(input.nextLine());
		if (0<lebar && lebar<=250) {
			lebar = lebar;
		}
		else {
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		System.out.print("Tinggi Tubuh (cm)      : ");
		int tinggi = Integer.parseInt(input.nextLine());
		if (0<tinggi && tinggi<=250) {
			tinggi = tinggi;
		}
		else {
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		System.out.print("Berat Tubuh (kg)       : ");
		double berat = Double.parseDouble(input.nextLine());
		if (0<berat && berat<= 150) {
			berat = berat;
		}
		else {
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		System.out.print("Jumlah Anggota Keluarga: ");
		int makanan = Integer.parseInt(input.nextLine());
		if (0<makanan && makanan<= 20) {
			makanan = makanan;
		}
		else {
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		System.out.print("Tanggal Lahir          : ");
		String tanggalLahir = input.nextLine();
		System.out.print("Catatan Tambahan       : ");
		String catatan = input.nextLine();
		System.out.print("Jumlah Cetakan Data    : ");
		int jumlahCetakan = Integer.parseInt(input.nextLine());


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// TODO Hitung rasio berat per volume (rumus lihat soal)
		int rasio = (int) (berat*100*100*100 / (panjang*lebar*tinggi));

		for (int nomor=1; nomor<=jumlahCetakan; nomor++) {
			// TODO Minta masukan terkait nama penerima hasil cetak data
			System.out.print("\nPencetakan " + nomor + " dari " + jumlahCetakan + " untuk: ");
			String penerima = input.nextLine().toUpperCase(); // Lakukan baca input lalu langsung jadikan uppercase

			// TODO Periksa ada catatan atau tidak
			// TODO Cetak hasil (ganti string kosong agar keluaran sesuai)
			String hasil = "DATA SIAP DICETAK UNTUK " + penerima + 
			"\n-----------------\n" + 
			nama + " - " + alamat + 
			"\nLahir pada tanggal " + tanggalLahir + 
			"\nRasio Berat Per Volume	= " + rasio + " kg/m^3\n"; 
			if (catatan.equals("")) {
				hasil += "Tidak ada catatan tambahan";
			}
			else {
				hasil += "Catatan: " + catatan;
			}
			
			System.out.println(hasil);
		}


		// TODO Bagian ini digunakan untuk soal bonus "Rekomendasi Apartemen"
		// TODO Hitung nomor keluarga dari parameter yang telah disediakan (rumus lihat soal)
		
		int ascii = 0;
		for (int i=0; i<nama.length(); i++) {
			char character = nama.charAt(i);
			int characterInt = (int)character;
			
			ascii += characterInt;
		}
		
		int nomor = (int) ((panjang * tinggi * lebar) + ascii) % 10000;

		// TODO Gabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
		String nomorKeluarga = (nama.charAt(0) + String.valueOf(nomor));

		// TODO Hitung anggaran makanan per tahun (rumus lihat soal)
		int anggaran = (int) (50000 * 365 * (makanan));

		// TODO Hitung umur dari tanggalLahir (rumus lihat soal)
		String tahunLahir = tanggalLahir.substring(6, 10); // lihat hint jika bingung
		int umur = (int) (2018-Integer.parseInt(tahunLahir));

		// TODO Lakukan proses menentukan apartemen (kriteria lihat soal)
		String namaApart = "";
		String kabupaten = "";
		
		if (0<=umur && umur<=18) {
			namaApart = "PPMT";
			kabupaten = "Rotunda";
		}
		else if (19<=umur && umur<=1018) {
			if (0<=anggaran && anggaran<=100000000) {
				namaApart = "Teksas";
				kabupaten = "Sastra";
			}
			else if (100000000<=anggaran) {
				namaApart = "Mares";
				kabupaten = "Margonda";
			}
		}
	

		// TODO Cetak rekomendasi (ganti string kosong agar keluaran sesuai)
		String rekomendasi = "\nREKOMENDASI APARTEMEN\n" + 
		"--------------------\n" +
		"MENGETAHUI: Identitas keluarga: " + nama + " - " + nomorKeluarga + 
		"\nMENIMBANG: Anggaran makanan tahunan: Rp" + anggaran +
		"\n\t\tUmur kepala keluarga: " + umur +" tahun\n" +
		"MEMUTUSKAN: keluarga " + nama + " akan ditempatkan di:\n" + 
		namaApart + ", kabupaten " + kabupaten;
		System.out.println(rekomendasi);

		input.close();
	}
}