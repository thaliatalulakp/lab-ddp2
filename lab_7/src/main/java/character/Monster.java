package character;

//  write Monster Class here
public class Monster extends Player{
    private String roar;
    public Monster(String name,  int hp){
        super(name, hp*2, "Monster");
        setRoar("AAAAAAaaaAAAAAaaaAAAAAA");
    }

    public Monster(String name, int hp, String roar){
        super(name, hp*2, "Monster");
        setRoar(roar);
    }

    public String roar() {return roar;}

    public void setRoar(String roar) {this.roar = roar;}
}