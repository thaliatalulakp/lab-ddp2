package character;

//  write Magician Class here
public class Magician extends Player{
    public Magician(String name, int hp){
        super(name, hp, "Magician");
    }

    public String burn(Player enemy){
        String burn = "";
        if (enemy.isDead() == false && enemy.isBaked() == false) {
            if (enemy.getType().equals("Magician")) {
                enemy.setHp(enemy.getHp() - 20);
                burn = "Nyawa " + enemy.getName() + " " + enemy.getHp();
                if (enemy.getHp() == 0) {
                    burn += "\ndan matang";
                    enemy.setBaked(true);
                }
            } else {
                enemy.setHp(enemy.getHp() - 10);
                burn = "Nyawa " + enemy.getName() + " " + enemy.getHp();
                if (enemy.getHp() == 0) {
                    burn += "\ndan matang";
                    enemy.setBaked(true);
                }
            }
        } else { //udah mati
            if (enemy.isBaked() == false) {
                burn = "Nyawa " + enemy.getName() + " " + enemy.getHp() + "\ndan matang";
                enemy.setBaked(true);
            }
        }
        return burn;
    }
}