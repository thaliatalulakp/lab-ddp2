package character;
import java.util.ArrayList;

//  write Player Class here
public class Player{
    private String name;
    private int hp;
    private String type;
    private boolean isDead;
    private boolean isBaked;
    private String roar;
    ArrayList<Player> diet = new ArrayList<Player>();

    public Player(String name, int hp, String type){
        this.name = name;
        this.hp = hp;
        this.type = type;
    }
    public String getName() {return name;}

    public void setName(String name) {this.name = name;}

    public int getHp() {
        if(hp < 0){
            hp = 0;
            return hp;
        }
        return hp;
    }

    public void setHp(int hp) {this.hp = hp;}

    public String getType() {return type;}

    public void setType(String type) {this.type = type;}

    public boolean isDead() {
        isDead = false;
        if (hp == 0){
            isDead = true;
        }
        return isDead;}

    public void setDead(boolean dead) {isDead = dead;}

    public boolean isBaked() {return isBaked;}

    public void setBaked(boolean baked) {isBaked = baked;}

    public String roar() {
        return roar;
    }

    public void setRoar(String roar) {
        this.roar = roar;
    }

    public ArrayList<Player> getDiet() {return diet;}

    public boolean canEat(Player enemy) {
        if ((this instanceof Human || this instanceof Magician) && enemy instanceof Monster) {
            if (enemy.isDead() == true && enemy.isBaked() == true) {
                return true;
            } else {
                return false;
            }
        } else if (this instanceof Monster) {
            if (enemy.getHp() == 0) {
                return true;
            }
        }
        return false;
    }

    public String attack(Player enemy){
        if (enemy instanceof Magician) {
            enemy.setHp(enemy.getHp() - 20);
            return "Nyawa " + enemy.getName() + " " + enemy.getHp();
        } else {
            enemy.setHp(enemy.getHp() - 10);
            return "Nyawa " + enemy.getName() + " " + enemy.getHp();
        }
    }
}