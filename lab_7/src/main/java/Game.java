import character.*;
import java.util.ArrayList;

public class Game{
    ArrayList<Player> player = new ArrayList<Player>();
    /**
     * Fungsi untuk mencari karakter
     * @param String name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name){
        for (Player elemen : player){
            if(elemen.getName().equals(name)){
                return elemen;
            }
        }
        return null;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp){
        Player addPlayer = find(chara);
        if (addPlayer == null) { //gaaada yg sama
            if (tipe.equals("Human")) {
                Human human = new Human(chara, hp);
                player.add(human);
            }
            else if (tipe.equals("Magician")) {
                Magician magician = new Magician(chara, hp);
                player.add(magician);
            }
            else if (tipe.equals("Monster")) {
                Monster monster = new Monster(chara, hp);
                player.add(monster);
            }
            return chara + " ditambah ke game";
        }
        else{
            return "Sudah ada karakter bernama " + chara;}
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @param String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar) {
        Player addPlayerRoar = find(chara);
        if (addPlayerRoar == null) { //gaaada yg sama
            if (tipe.equals("Human")) {
                Human human = new Human(chara, hp);
                player.add(human);
            }
            else if (tipe.equals("Magician")) {
                Magician magician = new Magician(chara, hp);
                player.add(magician);
            }
            else if (tipe.equals("Monster")) {
                Monster monster = new Monster(chara, hp, tipe);
                player.add(monster);
            }
            return chara + " ditambah ke game";
        }
        return "Sudah ada karakter bernama " + chara;
    }

    /**
     * fungsi untuk menghapus character dari game
     * @param String chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara){
        Player removePlayer = find(chara);
            if (removePlayer != null){
                player.remove(removePlayer);
                return chara + " dihapus dari game";
            }
        return "Tidak ada " + chara;
    }

    /**
     * fungsi untuk menampilkan status character dari game
     * @param String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public String status(String chara){
        Player statusChara = find(chara);
        String result = "";
        if (statusChara != null){
            if (statusChara.isDead() == false) { //belum mati
                result += statusChara.getType() + " " + statusChara.getName() + "\nHP: " + statusChara.getHp() + "\nMasih hidup" + "\n" + diet(chara);
            }
            else { //sudah mati
                result += statusChara.getType() + " " + statusChara.getName() + "\nHP: " + statusChara.getHp() + "\nSudah meninggal dunia dengan damai\n" + diet(chara);
            }
        }
        else {
            result += "Tidak ada " + chara;
        }
        return result;
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status(){
        String result = "";
        for (Player elemen : player){
            result += status(elemen.getName()) + "\n";
        }
        return result;
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     * @param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara) {
        Player charaPlayer = find(chara);
        String result = "";
        String temp = "";
        if (charaPlayer != null) {
            if (charaPlayer.getDiet().size() > 0) {
                for (Player i : charaPlayer.getDiet()) {
                    temp += i.getType() + " " + i.getName();
                }
                result += "Memakan " + temp;

            } else {
                    result += "Belum memakan siapa siapa";
            }
        } else {
            result += "Tidak ada " + chara;
        }
        return result;
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet(){
        String result = "";
        for (Player i : player){
            result += diet(i.getName());
        }
        return "Termakan : " + result + "\n";
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName){
        Player attackPlayer = find(meName);
        Player enemyAttackPlayer = find(enemyName);
        String result = "";
        if (attackPlayer != null && enemyAttackPlayer != null) {//kalau enemyName ada
            result += attackPlayer.attack(enemyAttackPlayer);
        }
        else{
            result += "Tidak ada " + attackPlayer + " atau " + enemyAttackPlayer;
        }
        return result;
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName) {
        Player burnPlayer = find(meName);
        Player enemyBurnPlayer = find(enemyName);
        String result = "";
        if (burnPlayer != null && enemyBurnPlayer != null){
            result += ((Magician)burnPlayer).burn(enemyBurnPlayer);
        }
        else {
            result += "Tidak ada "  + meName + " atau " + enemyName;
        }
        return result;
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName){
        Player eatPlayer = find(meName);
        Player enemyEatPlayer = find(enemyName);
        String result = "";
        if (eatPlayer != null && enemyEatPlayer != null){
            if (eatPlayer.canEat(enemyEatPlayer)){
                eatPlayer.setHp(eatPlayer.getHp() + 15);
                player.remove(enemyEatPlayer);
                eatPlayer.getDiet().add(enemyEatPlayer); //masukin enemy eat player ke list yg udah dimakan eatplayer
                result +=  eatPlayer.getName() + " memakan " + enemyEatPlayer.getName() + "\nNyawa " + eatPlayer.getName() + " kini " + eatPlayer.getHp();
            }
            else{
                result += eatPlayer.getName() + " tidak bisa memakan " + enemyEatPlayer.getName();
            }
        }
        else {
            result += "Tidak ada " + meName + " atau " + enemyName;
        }
        return result;
    }

     /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     * @param String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName){
        Player roarPlayer = find(meName);
        String result = "";
        if (roarPlayer != null){
            if (roarPlayer.getType().equals("Monster")){
                result += ((Monster)roarPlayer).roar();
            }
            else {
                result += meName + " tidak bisa berteriak";
            }
        }
        else {
            result += "Tidak ada " + meName;
        }
        return result;
    }
}